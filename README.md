RIP ADP App
===========

Intro
-----

**Notre point de départ :** une grosse indignation face au manque de médiatisation de la pétition sur le référendum ADP, et la difficulté pratique à signer la pétition pour ceux qui en ont entendu parler.

[adprip.fr](https://www.adprip.fr/signez.html) fait une bonne intro au sujet -- mais on trouve aussi des perles comme par exemple [Radio France refuse d’informer sur le Référendum ADP](https://la-bas.org/la-bas-magazine/la-bas-express/radio-france-refuse-d-informer-sur-le-referendum-adp). Enfin, en ces temps de grève et de manifs, vous voyez le tableau.

**Alors pour accélérer les choses :** sans forcément s'illusionner avec la barre des 4,7 millions qui sera dure à atteindre en 3 mois (mais on sait jamais !), on fait une interface alternative pour signer la pétition pour que ça devienne une affaire de deux clics facile à diffuser.

**Le projet :** une appli qui vous propose de prendre en photo le recto et le verso de votre carte d'identité ; elle reconnaît sur les photos la plupart des champs nécessaires à la signature (il manque juste le bureau de vote, qu'on fait à l'étape suivante), et elle envoie votre signature au [serveur du ministère de l'intérieur](https://www.referendum.interieur.gouv.fr/soutien/etape-1). Plus quelques fioritures pour recevoir des infos et partager avec ses amis sur les réseaux sociaux qu'on veut, ça pourrait changer la donne. Toute l'analyse des photos se passe sur le téléphone, donc pas de problème de vie privée.

Petit storyboard :

![storyboard](https://gitlab.com/ripadp/app/raw/master/storyboard.jpg?inline=false)

Current status
--------------

Il y a 4 gros boulots à faire :
- l'algorithme de reconnaissance des cartes d'identité, qu'on développe initialement en Python : [ripadp/id-recognition](https://gitlab.com/ripadp/id-recognition)
- le portage de cet algo vers le framework Flutter (modèle TensorFlow entraîné et appels à OpenCV et Tesseract) : [ripadp/protoapp-id-recognition](https://gitlab.com/ripadp/protoapp-id-recognition)
- l'interface de demande du bureau de vote, aussi en Flutter : [ripadp/protoapp-bureau-vote](https://gitlab.com/ripadp/protoapp-bureau-vote)
- l'ingénierie inverse du backend du ministère pour pouvoir envoyer les signatures (pas de repo pour l'instant)

Ensuite on aura juste à coller les briques ensemble, et voilà :). Donc pour l'instant il n'y a rien ici, et tout se passe dans les liens ci-dessus.

Contribuer
----------

Ouvrez les liens ci-dessus pour voir où ça en est. On peut aussi parler dans les issues, ou à @wehlutyk@{[mastodon.social](https://mastodon.social/@wehlutyk),[twitter.com](https://twitter.com/wehlutyk)}!
